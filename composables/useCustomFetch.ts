import type { UseFetchOptions } from "nuxt/app";
import { defu } from "defu";

export function useCustomFetch<T>(
  url: string,
  options: UseFetchOptions<T> = {}
) {
  const token = useCookie("token");
  const config = useRuntimeConfig();

  console.log("token", token);

  const defaults: UseFetchOptions<T> = {
    baseURL: config.public.apiBase,
    key: url,

    headers: token.value ? { Authorization: `Bearer ${token.value}` } : {},
  };

  const params = defu(options, defaults);

  return useFetch(url, params);
}
